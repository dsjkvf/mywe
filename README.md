MYWE
===

**My YR.NO Weather** is a simple Python (cgi) script to print out the outdoor temperature / conditions received from [YR.NO](https://www.yr.no) according to the provided settings.

Below is the example of calling it via JavaScript:

    function getMyWeather(z) {
        var the_url = "https://YOUR-PATH-TO/myyrnowe.cgi";
        var params = "city=" + z;
        var my_req = new XMLHttpRequest();
        my_req.open("POST", the_url, async=true);
        my_req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        my_req.onreadystatechange = function() {
            if(my_req.readyState == 4 && my_req.status == 200) {
                alert(my_req.responseText.split("\n").splice(4,29).join('\n'));
            }
        }
        my_req.send(params);
    }
