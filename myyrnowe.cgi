#!/usr/bin/env python
# coding: utf-8
import re
import cgi
import urllib2
from BeautifulSoup import BeautifulSoup
from myyrnowe import cities
# define the type of the output
print 'Content-type: text/html\n\n'
print '<head>'
print '<link rel="stylesheet" href="ns.css" type="text/css">'
print '</head>'
# helper to insert date sometimes
def date_time(z):
    date_time.counter += 1
    if date_time.counter == 1 or "00:00:00" in z:
        return "\n" + re.sub("T", ":\n", z)[0:-3]
    else:
        return z[-8:-3]
date_time.counter = 0
# get data
city = cgi.FieldStorage()["city"].value
for key in cities.keys():
    if city in key:
        city_url = cities[key]
        city_name = key
if city_url == '':
    print "Your request was ambiguous."
    quit()
print city_name[5:] + ":"
data = urllib2.urlopen(city_url)
# parse that data
soup = BeautifulSoup(data.read())
# collect and print attributes of interesta from that data
for values in soup.findAll("time"):
    print("{} : {}, {}°".format(date_time(values["from"]), values.find("symbol")["name"], values.find("temperature")["value"]))
